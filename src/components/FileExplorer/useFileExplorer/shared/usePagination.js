import React from 'react';

const hasPreviousPage = (page) => {
    return page !== 1
}

const hasNextPage = (page, totalPages) => {
    return page > 0 && page < totalPages
}

const getInitialPagedInfo = pagedInfo => {
    const {
        indexFrom,
        perPage,
    } = pagedInfo

    return {
        indexFrom,
        perPage,
        page: indexFrom,
        totalPages: 0,
        items: [],
        reloadToken: -1,
        isLoading: false
    }
}

const SET_PAGE = 'SET_PAGE'
const SET_TOTAL_PAGES = 'SET_TOTAL_PAGES'
const SET_PER_PAGE = 'SET_PER_PAGE'
const SET_ITEMS = 'SET_ITEMS'
const SET_RELOAD_TOKEN = 'SET_RELOAD_TOKEN'
const SET_IS_LOADING = 'SET_IS_LOADING'

function reducer(state, action) {
    switch (action.type) {
        case SET_PAGE:
            return { ...state, page: action.payload }
        case SET_TOTAL_PAGES:
            return { ...state, totalPages: action.payload }
        case SET_PER_PAGE:
            return { ...state, perPage: action.payload }
        case SET_ITEMS:
            return { ...state, items: action.payload }
        case SET_RELOAD_TOKEN:
            return { ...state, reloadToken: action.payload }
        case SET_IS_LOADING:
            return { ...state, isLoading: action.payload }

        default: throw new Error()
    }
}


const usePagination = (config) => {
    const {
        promiseFn,
        indexFrom = 1,
        perPage = 20,
        reduceItems = x => x
    } = config

    if (!promiseFn) {
        throw new Error("[promiseFn] is required")
    }

    const initialState = getInitialPagedInfo({ indexFrom, perPage })

    const [state, dispatch] = React.useReducer(reducer, initialState)

    React.useEffect(() => {
        if (state.reloadToken === -1) return

        async function fn() {
            dispatch({ type: SET_IS_LOADING, payload: true })

            try {
                const response = await promiseFn({ page: state.page, perPage: state.perPage })

                if (!response.safe.ok) {
                    dispatch({ type: SET_PAGE, payload: initialState.page })
                    dispatch({ type: SET_TOTAL_PAGES, payload: initialState.totalPages })
                    dispatch({ type: SET_ITEMS, payload: initialState.items })
                    dispatch({ type: SET_PER_PAGE, payload: initialState.perPage })

                    return
                }

                const {
                    page,
                    totalPages,
                    data,
                    perPage
                } = response.data.result
                
                if (page > totalPages && totalPages !== 0) {
                    setPaging({ page: 1 })
                    return
                }

                dispatch({ type: SET_PAGE, payload: page })
                dispatch({ type: SET_TOTAL_PAGES, payload: totalPages })
                dispatch({ type: SET_ITEMS, payload: reduceItems(data) })
                dispatch({ type: SET_PER_PAGE, payload: perPage })

            } finally {
                dispatch({ type: SET_IS_LOADING, payload: false })
            }
        }

        fn();
    }, [state.reloadToken])

    function setPaging(pagedInfo = {}) {
        if (pagedInfo.page !== undefined) {
            dispatch({ type: SET_PAGE, payload: pagedInfo.page })
        }

        if (pagedInfo.perPage !== undefined) {
            dispatch({ type: SET_PER_PAGE, payload: pagedInfo.perPage })
        }

        dispatch({ type: SET_RELOAD_TOKEN, payload: Math.random() })
    }

    return {
        ...state,

        setPaging,

        hasPreviousPage: hasPreviousPage(state.page),
        hasNextPage: hasNextPage(state.page, state.totalPages)
    }
}

export default usePagination