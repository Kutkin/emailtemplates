import React from 'react'

function useHierarchy() {
    const [routes, setRoutes] = React.useState([])

    return {
        routes,

        resetRoutes() {
            setRoutes([])
        },

        refreshRoutes(id, text = "No title") {
            let result
            const index = routes.findIndex(x => x.id === id)
            if (index === -1) {
                routes.push({ id, text })
                result = routes
            } else {
                result = routes.slice(0, index + 1)
            }
            setRoutes([...result])
        },

        getLastRoute() {
            const lastRoute = routes[routes.length - 1]
            return lastRoute || {}
        }
    }

}

export default useHierarchy