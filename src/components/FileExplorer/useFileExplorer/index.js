import React from 'react'

import usePagination from './shared/usePagination'
import useHierarchy from './shared/useHierarchy'
import useUpdateEffect from 'shared/useUpdateEffect'

function useFileExplorer({
    folderReduceItems = x => x,
    fileReduceItems = x => x,
    perPage,
    folderPromiseFn,
    filePromiseFn,
}) {
    const hierarchy = useHierarchy()
    const parentFolderId = hierarchy.getLastRoute().id

    const folderPagination = usePagination({
        promiseFn: x => folderPromiseFn({ ...x, parentFolderId }),
        reduceItems: folderReduceItems,
        perPage
    })

    const filePagination = usePagination({
        promiseFn: x => filePromiseFn({ ...x, parentFolderId }),
        reduceItems: fileReduceItems,
        perPage
    })

    useUpdateEffect(() => {
        folderPagination.setPaging({ page: 1 })
        filePagination.setPaging({ page: 1 })
    }, [parentFolderId])

    function reset() {
        hierarchy.resetRoutes()
        folderPagination.setPaging({ page: 1 })
        filePagination.setPaging({ page: 1 })
    }

    return {
        hierarchy,
        folderPagination,
        filePagination,

        sync() {
            folderPagination.setPaging()
            filePagination.setPaging()
        },

        reset
    }
}

export default useFileExplorer