import React from 'react'
import { Icon, Tag } from "@blueprintjs/core"
import { IconNames } from "@blueprintjs/icons"
import { Flex } from 'reflexbox'

function HierarchyPath({ routes, onRouteClick }) {

    const hasChildrenRoutes = routes.length !== 0

    return (
        <Flex
            alignItems="center"
        >
            <Tag
                minimal
                interactive={hasChildrenRoutes}
                large
                rightIcon={IconNames.HOME}
                onClick={() => hasChildrenRoutes && onRouteClick({ id: null })}
            >
                В начало
            </Tag>
            {hasChildrenRoutes && (
                <Icon
                    icon={IconNames.CHEVRON_RIGHT}
                    iconSize={Icon.SIZE_STANDARD}
                />
            )}
            {routes.map((x, i) => {
                const isLast = i === routes.length - 1

                return (
                    <Flex
                        key={x.id}
                        alignItems="center"
                    >
                        <Tag
                            minimal
                            interactive={!isLast}
                            rightIcon={IconNames.FOLDER_OPEN}
                            onClick={() => !isLast && onRouteClick(x)}
                        >
                            {x.text}
                        </Tag>
                        
                        {!isLast && (
                            <Icon
                                icon={IconNames.CHEVRON_RIGHT}
                                iconSize={Icon.SIZE_STANDARD}
                            />
                        )}
                    </Flex>
                )
            })}
        </Flex>
    )
}

export default HierarchyPath