/** @jsx jsx */
import { jsx, css } from '@emotion/core'
import React from 'react'
import { HTMLTable, Button, Icon } from "@blueprintjs/core"
import { IconNames } from "@blueprintjs/icons"
import orderBy from 'lodash.orderby'
import { Flex } from 'reflexbox'

import { Loading } from './Loading'
import { Empty } from './Empty'
import { Tr } from './Tr'
import { TableHeader } from './TableHeader'
import { MARGIN } from 'shared/theme'

function Table(props) {
    const {
        columns,
        actions: Actions,
        setFilter,

        items,
        isLoading,
        hasNextPage,
        hasPreviousPage,
        setPaging,
        page,
        totalPages,
    } = props

    const [displayItems, setDisplayItems] = React.useState(items)

    React.useEffect(() => {
        setDisplayItems([...items])
    }, [items])

    return (
        <>
            <Flex flexWrap="wrap" width="38rem" height="28rem" css={css`position:relative;`} >
                {isLoading && <Loading />}
                {!isLoading && displayItems.length === 0 && <Empty />}
                <HTMLTable
                    striped
                    condensed
                    bordered
                    width="100%"
                >
                    <thead>
                        <tr>
                            {columns.map(x => {
                                const { Cell, ...otherCellProps } = x
                                return (
                                    <TableHeader
                                        key={x.name}
                                        cell={otherCellProps}
                                        shouldReset={page}
                                        onSort={(dir, propName) => {
                                            const sortedItems = orderBy(displayItems, [propName], [dir])
                                            setDisplayItems(sortedItems)
                                        }}
                                    >
                                        <Cell
                                            cell={otherCellProps}
                                            displayItems={displayItems}
                                            setFilter={setFilter}
                                        />
                                    </TableHeader>
                                )
                            })}
                        </tr>
                    </thead>
                    <tbody>
                        {displayItems.map(item => {
                            return (
                                <Tr key={item.id}>
                                    {columns.map((column, i) => {
                                        return (
                                            <td key={i}>
                                                {item[column.name]}
                                            </td>
                                        )
                                    })}
                                    <td>
                                        <Actions selectedItem={item} />
                                    </td>
                                </Tr>
                            )
                        })}
                    </tbody>
                </HTMLTable>
            </Flex>
            <Flex
                flexGrow={1}
                mt={MARGIN.NORMAL}
                justifyContent="center"
                alignItems="center"
            >
                <Button
                    css={css`margin-right:1rem;`}
                    disabled={!hasPreviousPage || isLoading}
                    onClick={() => setPaging({ page: page - 1 })}
                >
                    <Icon
                        css={css`padding-right:1rem;`}
                        icon={IconNames.CHEVRON_LEFT}
                    />
                    Предыдущая страница
                </Button>
                <Button
                    disabled={!hasNextPage || isLoading}
                    onClick={() => setPaging({ page: page + 1 })}
                >
                    Следующая страница
                   <Icon
                        css={css`padding-left:1rem;`}
                        icon={IconNames.CHEVRON_RIGHT}
                    />
                </Button>
            </Flex>
            <Flex mt={MARGIN.SMALL} justifyContent="center">
                страница {page} из {totalPages || 1}
            </Flex>
        </>
    )
}

export default Table