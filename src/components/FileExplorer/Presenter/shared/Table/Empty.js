import React from 'react'
import { NonIdealState } from "@blueprintjs/core"
import { IconNames } from "@blueprintjs/icons"

export function Empty() {
    return (<div style={{ position: "absolute", width: '100%', top: '30%' }}>
        <NonIdealState icon={IconNames.SEARCH} title="Нет элементов" />
    </div>)
}
