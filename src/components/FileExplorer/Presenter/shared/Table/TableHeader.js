import React from 'react'
import { Icon, Tag } from "@blueprintjs/core"
import { IconNames } from "@blueprintjs/icons"
import { Flex } from 'reflexbox'

export function TableHeader({ children, onSort, cell, shouldReset }) {
    const [isAsc, setIsAsc] = React.useState(true)

    React.useEffect(() => {
        setIsAsc(true)
    }, [shouldReset])

    return (<td width={cell.width}>
        <Flex justifyContent="center" alignItems="center" flexWrap="wrap" width="100%">
            <Tag
                minimal
                interactive
                rightIcon={<Icon icon={isAsc ? IconNames.ARROW_UP : IconNames.ARROW_DOWN} />
                } onClick={() => {
                    const dir = !isAsc ? 'asc' : 'desc';
                    setIsAsc(x => !x);
                    onSort(dir, cell.name);
                }}>
                {cell.displayName}
            </Tag>
        </Flex>
        {children}
    </td>)
}
