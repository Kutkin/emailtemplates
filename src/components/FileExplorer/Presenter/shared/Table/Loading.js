import React from 'react'
import { ProgressBar } from "@blueprintjs/core"

export function Loading() {
    return (<div style={{ position: "absolute", width: '100%', bottom: 0 }}>
        <ProgressBar />
    </div>)
}
