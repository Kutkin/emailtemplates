import React from 'react'
import styled from '@emotion/styled'

export const Tr = styled.tr(props => ({
    backgroundColor: props.isSelected ? '#CED9E0' : 'auto'
}))
