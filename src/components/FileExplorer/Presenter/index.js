import React from 'react'
import { Flex, Box } from 'reflexbox'
import { Card, Divider, H2, H3, Button, Intent } from '@blueprintjs/core'

import FolderTable from './FolderTable'
import FileTable from './FileTable'
import HierarchyPath from './HierarchyPath'
import { PALETTE, MARGIN } from 'shared/theme'

function FileExplorerPresenter(props) {
    return (
        <Card>
            <Flex
                flexWrap="wrap"
                justifyContent="space-between"
                maxWidth="85rem"
            >
                <Box width={[1]}>
                    <H2>
                        Менеджер шаблонов
                    </H2>
                    <Divider />
                </Box>
                <Box
                    p={MARGIN.LARGE}
                    backgroundColor={PALETTE.PRIMARY}
                >
                    <Flex height="3rem" alignItems="center">
                        <Box mr={MARGIN.NORMAL}>
                            <H3>Категории</H3>
                        </Box>
                        <HierarchyPath
                            routes={props.hierarchy.routes}
                            onRouteClick={({ id, text }) => {
                                id === null ?
                                    props.hierarchy.resetRoutes() :
                                    props.hierarchy.refreshRoutes(id, text)
                            }}
                        />
                    </Flex>
                    <Box mt={MARGIN.NORMAL}>
                        <Button
                            intent={Intent.PRIMARY}
                            onClick={props.onFolderCreate}
                            small
                        >
                            Создать категорию
                    </Button>
                    </Box>
                    <Box mt={MARGIN.NORMAL}>
                        <FolderTable {...props} />
                    </Box>
                </Box>
                <Box
                    backgroundColor={PALETTE.SECONDARY}
                    p={MARGIN.LARGE}
                >
                    <Flex height="3rem" alignItems="center">
                        <H3>Сообщения</H3>
                    </Flex>
                    <Box mt={MARGIN.NORMAL}>
                        <Button
                            intent={Intent.PRIMARY}
                            onClick={props.onMessageCreate}
                            small
                        >
                            Создать сообщение
                    </Button>
                    </Box>
                    <Box mt={MARGIN.NORMAL}>
                        <FileTable {...props} />
                    </Box>
                </Box>
            </Flex>
        </Card>
    )
}

export default FileExplorerPresenter