import React from 'react'
import { Flex } from "reflexbox"
import CommonFilter from 'components/FiltersManager/CommonFilter'
import { MARGIN } from 'shared/theme'

const columns = [
    {
        name: "id",
        displayName: "id",
        Cell: () => null,
        width: '70px'
    },
    {
        name: "title",
        displayName: "Название",
        width: '230px',
        Cell: props => {
            return (
                <Flex mt={MARGIN.SMALL} justifyContent="center">
                    <CommonFilter
                        handleSubmit={({ value }, formikBag) => {
                            const query = value ? ["title", "LIKE", value] : []
                            props.setFilter({ name: 'findMessageByName', query, actions: { reset: formikBag.resetForm } })
                        }}
                    />
                </Flex>
            )
        }
    },
]


export default columns