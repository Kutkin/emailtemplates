import React from 'react'

import Table from 'components/FileExplorer/Presenter/shared/Table'
import columns from 'components/FileExplorer/Presenter/FileTable/columns'

function FileTable(props) {
    return (
        <Table
            columns={columns}
            actions={props.fileActions}
            setFilter={props.setMessagesFilter}
            {...props.filePagination}
        />
    )

}


export default FileTable