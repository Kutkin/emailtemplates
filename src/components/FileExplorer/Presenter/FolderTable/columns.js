import React from 'react'
import { Flex } from "reflexbox"
import CommonFilter from 'components/FiltersManager/CommonFilter'
import QuantityFilter from 'components/FiltersManager/QuantityFilter'
import { MARGIN } from 'shared/theme'

const columns = [
    {
        name: "id",
        displayName: "id",
        Cell: () => null,
        width: '70px'
    },
    {
        name: "title",
        displayName: "Название",
        Cell: props => {
            return (
                <Flex mt={MARGIN.SMALL} justifyContent="center">
                    <CommonFilter
                        handleSubmit={({ value }, formikBag) => {
                            const query = value ? ["title", "LIKE", value] : []
                            props.setFilter({ name: 'findCategoryByName', query, actions: { reset: formikBag.resetForm } })
                        }}
                    />
                </Flex>
            )
        },
        width: '230px'
    },
    {
        name: "messagesCount",
        displayName: "Кол-во сообщений",
        Cell: (props) => (
            <Flex mt={MARGIN.SMALL} justifyContent="center">
                <QuantityFilter
                    handleSubmit={({ value }, formikBag) => {
                        const query = value ? ["messages", value.slice(0, 1), value.slice(2)] : []
                        props.setFilter({ name: 'findCategoryMessagesCount', query, actions: { reset: formikBag.resetForm } })
                    }}
                />
            </Flex>
        ),
        width: '70px'
    }
]

export default columns