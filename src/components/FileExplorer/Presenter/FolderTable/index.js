import React from 'react'
import Table from '../shared/Table'
import columns from 'components/FileExplorer/Presenter/FolderTable/columns'

function FolderTable(props) {
    return (
        <Table
            columns={columns}
            actions={props.folderActions}
            setFilter={props.setCategoriesFilter}
            {...props.folderPagination}
        />
    )

}

export default FolderTable