/** @jsx jsx */
import { jsx } from '@emotion/core'
import styled from '@emotion/styled'
import React from 'react'
import NavigationBar from './NavigationBar'

const headerHeight = "50px";

const Shell = styled.section(
    {
        height: '100vh',
        display: 'flex',
        flexWrap: 'wrap',
        overflowY: 'hidden'
    }
)

const Header = styled.section(
    {
        height: headerHeight,
        width: '100%'
    }
)

const Body = styled.section(
    {
        height: `calc(100vh - ${headerHeight})`,
        width: `100vw`,
        backgroundColor: '#f4f5f7',
        overflowY: 'auto'
    }
)

const DefaultLayout = ({ children }) => (
    <Shell>
        <Header>
            <NavigationBar />
        </Header>
        <Body>
            {children}
        </Body>
    </Shell>
)

export default DefaultLayout