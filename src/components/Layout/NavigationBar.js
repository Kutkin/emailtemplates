import React from 'react'
import {
    Alignment,
    Button,
    Classes,
    Navbar,
    NavbarGroup,
    NavbarHeading,
} from "@blueprintjs/core";


function NavigationBar(props) {
    return (
        <Navbar>
            <NavbarGroup css={{ width: '100%' }} align={Alignment.LEFT}>
                <Button css={{ fontWeight: 700, ".bp3-navbar-heading": { marginRight: '0rem' } }}
                    className={Classes.MINIMAL}
                >
                    <NavbarHeading>
                        Шаблоны почты
                    </NavbarHeading>
                </Button>
            </NavbarGroup>
        </Navbar>
    )
}

export default NavigationBar