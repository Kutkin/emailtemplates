/** @jsx jsx */
import { jsx, css } from '@emotion/core'
import React from 'react'
import { Classes, Dialog } from "@blueprintjs/core"

const config = {
    autoFocus: true,
    canEscapeKeyClose: false,
    canOutsideClickClose: false,
    enforceFocus: true,

    usePortal: true,
    lazy: true
}

class PeDialog extends React.PureComponent {

    render() {
        const {
            icon: Icon,
            header: Header,
            cssPresets = css`padding-bottom:0`,
            ...otherProps
        } = this.props

        return (
            <Dialog
                icon={Icon}
                title={<Header />}
                css={cssPresets}
                transitionDuration={500}
                {...config}
                {...otherProps}
            >
                <div className={Classes.DIALOG_BODY}>
                    {this.props.children}
                </div>
            </Dialog>
        )
    }
}

export default PeDialog