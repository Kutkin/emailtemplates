import React from 'react'

const Query = props => {
    const {
        promiseFn,
        children
    } = props

    const [isLoading, setLoading] = React.useState(true)
    const [answer, setAnswer] = React.useState()

    React.useEffect(() => {
        async function fn() {
            const response = await promiseFn()
            setAnswer(response)
            setLoading(false)
        }

        fn()
    }, [])

    return children({
        answer,
        isLoading,
    });
}

export {
    Query
}