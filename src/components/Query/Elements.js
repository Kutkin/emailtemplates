import React from 'react'
import styled from '@emotion/styled'
import { Intent, Spinner, NonIdealState } from "@blueprintjs/core"
import { IconNames } from "@blueprintjs/icons"

export const SpinnerShell = styled.div(props => {
    return {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: props.height,
        width: props.width,
    }
})

export const ErrorShell = styled.div(props => {
    return {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: props.height,
        width: props.width,
    }
})

export const Loader = props => {
    const {
        height,
        width
    } = props;
    return (
        <SpinnerShell height={height} width={width}>
            <Spinner intent={Intent.PRIMARY} size={Spinner.SIZE_STANDARD} />
        </SpinnerShell>
    )
}

export const Error = ({ state, height, width }) => {
    const {
        status = '',
        statusText = '',
    } = state

    return (
        <ErrorShell height={height} width={width}>
            <NonIdealState
                icon={IconNames.WRENCH}
                title={status}
                description={statusText}
            />
        </ErrorShell>
    )
}


