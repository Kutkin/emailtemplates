/** @jsx jsx */
import React from 'react';
import { jsx } from '@emotion/core'
import { Query } from './BaseQuery'
import { Loader, Error } from './Elements';

export default function QueryComponent(props) {
    const {
        children,
        height = '10rem',
        ...otherProps
    } = props;

    return (
        <Query {...otherProps}>
            {({ isLoading, answer }) => {
                if (isLoading) return <Loader height={height} width='100%' />

                const isMultiple = Array.isArray(answer)
                if (isMultiple) {
                    const fail = answer.some(x => !x.safe.ok)
                    return fail ? <Error state={answer} height={height} width='100%' /> : children(answer)
                }

                if (!answer.safe.ok) return <Error state={answer} height={height} width='100%' />
                if (answer.data)
                    return children(answer)
                return null
            }}
        </Query>
    )
}