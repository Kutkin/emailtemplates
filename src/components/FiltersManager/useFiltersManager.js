import React from 'react'
import useUpdateEffect from 'shared/useUpdateEffect'
import useStateWithCallback from 'shared/useStateWithCallback'

export default function useFiltersManager(filters) {
    const [complexFilter, setComplexFilter] = useStateWithCallback(filters, () => {
        setSkipUpdate(false)
    })

    const [updateHash, setUpdateHash] = React.useState(-1)
    const [skipUpdate, setSkipUpdate] = React.useState(false)

    useUpdateEffect(() => {
        !skipUpdate && setUpdateHash(Math.random())
    }, [
        ...complexFilter.map(x => x.query)
    ])

    function resetFilters() {
        setSkipUpdate(true)

        const emptyFilters = complexFilter.map(x => x.reset())
        setComplexFilter([...emptyFilters])
    }

    function setFilter({ name, query, actions }) {
        const currentFilter = complexFilter.find(x => x.name === name)
        currentFilter.query = query
        currentFilter.actions = actions
        setComplexFilter([...complexFilter])
    }

    function getAllQueries() {
        return complexFilter.filter(x => x.query.length !== 0).map(x => x.query)
    }

    function createFilter() {
        let count = 1
        const queries = getAllQueries()
        let predicate = {}
        queries.map((q, i) => {
            predicate[`operand_${count}`] = q
            count = count + 1
        })

        if (queries.length === 1) {
            predicate[`operand_2`] = []
        }

        return predicate
    }

    return {
        updateHash,
        resetFilters,
        createFilter,
        filters: complexFilter,
        setFilter,
        getAllQueries,
    }
}