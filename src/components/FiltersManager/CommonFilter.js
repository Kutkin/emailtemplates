import React from 'react'
import { Form, withFormik, ErrorMessage } from 'formik'
import { InputGroup } from "@blueprintjs/core"

function CommonFilter(props) {
    return (
        <Form>
            <InputGroup
                small
                name="value"
                onChange={props.handleChange}
                onBlur={props.handleBlur}
                value={props.values.value}
                placeholder="полное название"
            />
            <ErrorMessage name="value" />
        </Form>
    )
}

const CommonFilterForm = withFormik({
    mapPropsToValues: props => ({
        value: ''
    }),
    handleSubmit: (values, formikBag) => formikBag.props.handleSubmit(values, formikBag),
})(CommonFilter)


export default CommonFilterForm