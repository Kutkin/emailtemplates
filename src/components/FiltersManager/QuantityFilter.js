import React from 'react'
import { Form, withFormik } from 'formik'
import { Intent, InputGroup } from "@blueprintjs/core"
import * as Yup from 'yup';

const quantitySchema = Yup.object().shape({
    value: Yup.string()
        .matches(/^[><=]\s\d+$/, { excludeEmptyString: true }) // allow "[> = <] [number]"
})

function QuantityFilter(props) {
    return (
        <Form>
            <InputGroup
                name="value"
                onChange={props.handleChange}
                onBlur={props.handleBlur}
                value={props.values.value}
                intent={props.isValid ? Intent.NONE : Intent.DANGER}
                placeholder="шаблон: [> 12]"
                small
            />
        </Form>
    )
}

const QuantityFilterForm = withFormik({
    mapPropsToValues: props => ({
        value: ''
    }),
    handleSubmit: (values, formikBag) => formikBag.props.handleSubmit(values, formikBag),
    validationSchema: quantitySchema
})(QuantityFilter)


export default QuantityFilterForm