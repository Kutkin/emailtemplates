export default class Filter {
    
    constructor({name, initialQuery = []}) {
        this.name = name
        this.initialQuery = initialQuery
        this.query = initialQuery
        this.actions = {reset: () => {}}
        
    }

    reset() {
        this.query = this.initialQuery
        this.actions.reset()
        return this
    }
}