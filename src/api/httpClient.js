import axios from "axios"
import errorsInterceptor from './errorsInterceptor'

const httpClient = (baseURL, customOptions = {}) => {
    const options = {
        ...{
            baseURL,
            headers: {
                "Content-Type": "application/json"
            }
        },
        ...customOptions
    }

    const instance = axios.create(options)
    instance.interceptors.response.use(errorsInterceptor)

    return instance
}

export default httpClient