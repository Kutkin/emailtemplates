function errorsInterceptor(response) {
    response.safe = { ok: true }

    if (response.data.error || response.status !== 200) {
        response.safe.ok = false
    }

    return response
}

export default errorsInterceptor