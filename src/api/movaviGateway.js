import httpClient from './httpClient'

class MovaviGateway {

    constructor(baseUrl) {
        this.server = httpClient(baseUrl)

        this.readCategoriesPaged = this.readCategoriesPaged.bind(this)
        this.getCategory = this.getCategory.bind(this)
        this.updateCategory = this.updateCategory.bind(this)
        this.deleteCategory = this.deleteCategory.bind(this)

        this.readMessagesPaged = this.readMessagesPaged.bind(this)
        this.getMessage = this.getMessage.bind(this)
        this.updateMessage = this.updateMessage.bind(this)
        this.deleteMessage = this.deleteMessage.bind(this)
    }

    readCategoriesPaged({ page, perPage, conditions }) {
        return this.server.post('', { jsonrpc: "2.0", method: "readCategory", params: { page, perPage, conditions, fields: ['id', "title", "parent", 'messages'] } })
    }

    getCategory(id) {
        return this.server.post('', { jsonrpc: "2.0", method: "readCategory", params: { conditions: ['id', '=', id], fields: ['id', 'title', 'parent', 'children', 'messages'] } })
    }

    updateCategory(data) {
        return this.server.post('', { jsonrpc: "2.0", method: "updateCategory", params: { conditions: ['id', '=', data.id], data } })
    }

    createCategory(data) {
        return this.server.post('', { jsonrpc: "2.0", method: "createCategory", params: { data } })
    }

    deleteCategory(id) {
        return this.server.post('', { jsonrpc: "2.0", method: "deleteCategory", params: { conditions: ['id', '=', id] } })
    }

    readMessagesPaged({ page, perPage, conditions }) {
        return this.server.post('', { jsonrpc: "2.0", method: "readMessage", params: { page, perPage, conditions, fields: ['id', "title", "body"] } })
    }

    getMessage(id) {
        return this.server.post('', { jsonrpc: "2.0", method: "readMessage", params: { conditions: ['id', '=', id], fields: ['id', 'title', 'body', 'category'] } })
    }

    updateMessage(data) {
        return this.server.post('', { jsonrpc: "2.0", method: "updateMessage", params: { conditions: ['id', '=', data.id], data } })
    }

    createMessage(data) {
        return this.server.post('', { jsonrpc: "2.0", method: "createMessage", params: { data } })
    }

    deleteMessage(id) {
        return this.server.post('', { jsonrpc: "2.0", method: "deleteMessage", params: { conditions: ['id', '=', id] } })
    }
}

export default new MovaviGateway('https://simple-api.sandbox.movavi.com/api/v1/')