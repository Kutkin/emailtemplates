import React from 'react'
import { Flex, Box } from 'reflexbox'
import { Button, Intent } from '@blueprintjs/core'
import { IconNames } from "@blueprintjs/icons"

import movaviGateway from 'api/movaviGateway'
import Query from 'components/Query'
import Dialog from 'components/Dialog'
import FileExplorerPresenter from 'components/FileExplorer/Presenter'
import useHomePage from 'pages/HomePage/useHomePage'
import EditMessageForm from 'pages/HomePage/dependencies/messages/EditMessageForm'
import DeleteMessageForm from 'pages/HomePage/dependencies/messages/DeleteMessageForm'
import EditCategoryForm from 'pages/HomePage/dependencies/categories/EditCategoryForm'
import DeleteFolderForm from 'pages/HomePage/dependencies/categories/DeleteCategoryForm'
import FormMappers from './dependencies/formMappers'
import { MARGIN } from 'shared/theme'

const HomePage = () => {
    const {
        isFileEditModalOpen,
        toggleFileEditInputModal,
        openFileEditModal,
        fileEditId,

        isFileAddModalOpen,
        toggleFileAddInputModal,

        isFileDeleteModalOpen,
        toggleFileDeleteModal,
        openFileDeleteModal,

        folderId,
        toggleFolderEditModal,
        openFolderEditModal,
        isFolderEditModalOpen,

        isFolderAddModalOpen,
        toggleFolderAddModal,

        openFolderDeleteModal,
        isFolderDeleteModalOpen,
        toggleFolderDeleteModal,

        openFolder,

        fileExplorerApi,
        categoryConditionsApi,
        messageConditionsApi
    } = useHomePage()

    return (
        <Flex
            mt={MARGIN.LARGE}
            justifyContent="center"
        >
            <FileExplorerPresenter
                {...fileExplorerApi}
                setCategoriesFilter={categoryConditionsApi.setFilter}
                setMessagesFilter={messageConditionsApi.setFilter}
                onMessageCreate={() => toggleFileAddInputModal()}
                onFolderCreate={() => toggleFolderAddModal()}
                fileActions={({ selectedItem }) => {
                    return (
                        <Flex>
                            <Box mr={MARGIN.SMALL}>
                                <Button
                                    small
                                    icon={IconNames.EDIT}
                                    onClick={() => openFileEditModal(selectedItem.id)}
                                />
                            </Box>
                            <Button
                                small
                                icon={IconNames.TRASH}
                                intent={Intent.DANGER}
                                onClick={() => openFileDeleteModal(selectedItem.id)}
                            />
                        </Flex>
                    )
                }}
                folderActions={({ selectedItem }) => {
                    return (
                        <Flex>
                            <Box mr={MARGIN.NORMAL}>
                                <Button
                                    small
                                    icon={IconNames.FOLDER_OPEN}
                                    onClick={() => {
                                        openFolder(selectedItem.id, selectedItem.title)
                                    }}
                                >
                                    Открыть
                            </Button>
                            </Box>
                            <Box mr={MARGIN.NORMAL}>
                                <Button
                                    small
                                    icon={IconNames.EDIT}
                                    onClick={() => {
                                        openFolderEditModal(selectedItem.id)
                                    }}
                                />
                            </Box>
                            <Button
                                small
                                icon={IconNames.TRASH}
                                intent={Intent.DANGER}
                                onClick={() => openFolderDeleteModal(selectedItem.id)}
                            />
                        </Flex>
                    )
                }}
            />

            {/* FILES */}

            <Dialog
                icon={IconNames.ADD}
                isOpen={isFileAddModalOpen}
                header={() => <span>Создание сообщения</span>}
                onClose={toggleFileAddInputModal}
            >
                <EditMessageForm
                    initialModel={FormMappers.toCreateMessageForm(fileExplorerApi.hierarchy.getLastRoute())}
                    onClose={toggleFileAddInputModal}
                    handleSubmit={async (values) => {
                        const response = await movaviGateway.createMessage(FormMappers.messageToServer(values))
                        if (!response.safe.ok) return
                        
                        toggleFileAddInputModal()
                        fileExplorerApi.sync()
                    }}
                />
            </Dialog>

            <Dialog
                icon={IconNames.EDIT}
                isOpen={isFileEditModalOpen}
                header={() => <span>Редактирование сообщения</span>}
                onClose={toggleFileEditInputModal}
            >
                <Query height="400px" promiseFn={async () => {
                    const messageResponse = await movaviGateway.getMessage(fileEditId)
                    const categoryResponse = await movaviGateway.getCategory(messageResponse.data.result.data[0].category.id)
                    return [messageResponse, categoryResponse]
                }}>
                    {responses => (
                        <EditMessageForm
                            initialModel={FormMappers.toEditMessageForm(responses)}
                            onClose={toggleFileEditInputModal}
                            handleSubmit={async (values) => {
                                const response = await movaviGateway.updateMessage(FormMappers.messageToServer(values))
                                if (!response.safe.ok) return

                                toggleFileEditInputModal()
                                fileExplorerApi.sync()
                            }}
                        />
                    )}
                </Query>
            </Dialog>

            <Dialog
                icon={IconNames.TRASH}
                isOpen={isFileDeleteModalOpen}
                header={() => <span>Удаление сообщения</span>}
                onClose={toggleFileDeleteModal}
            >
                <DeleteMessageForm
                    onClose={toggleFileDeleteModal}
                    handleSubmit={async () => {
                        const response = await movaviGateway.deleteMessage(fileEditId)
                        if (!response.safe.ok) return

                        toggleFileDeleteModal()
                        fileExplorerApi.sync()
                    }}
                />
            </Dialog>

            {/* FOLDERS */}

            <Dialog
                icon={IconNames.EDIT}
                isOpen={isFolderEditModalOpen}
                header={() => <span>Редактирование категории</span>}
                onClose={toggleFolderEditModal}
            >
                <Query height="300px" promiseFn={async () => {
                    const currentCategoryResponse = await movaviGateway.getCategory(folderId)
                    if (!currentCategoryResponse.safe.ok) return

                    const { parent } = currentCategoryResponse.data.result.data[0]
                    let parentCategoryResponse
                    if (parent) {
                        parentCategoryResponse = await movaviGateway.getCategory(parent.id)
                        if (!parentCategoryResponse.safe.ok) return

                        return [currentCategoryResponse, parentCategoryResponse]
                    }

                    return [currentCategoryResponse]

                }}>
                    {responses => (
                        <EditCategoryForm
                            initialModel={FormMappers.categoryToForm(responses)}
                            onClose={toggleFolderEditModal}
                            handleSubmit={async (values) => {
                                const response = await movaviGateway.updateCategory(FormMappers.formToCategory(values))
                                if (!response.safe.ok) return

                                toggleFolderEditModal()
                                fileExplorerApi.sync()
                            }}
                        />
                    )}
                </Query>
            </Dialog>

            <Dialog
                icon={IconNames.ADD}
                isOpen={isFolderAddModalOpen}
                header={() => <span>Создать категорию</span>}
                onClose={toggleFolderAddModal}
            >
                <EditCategoryForm
                    initialModel={FormMappers.createCategory()}
                    onClose={toggleFolderAddModal}
                    handleSubmit={async (values) => {
                        const response = await movaviGateway.createCategory(FormMappers.formToCategory(values))
                        if (!response.safe.ok) return

                        toggleFolderAddModal()
                        fileExplorerApi.sync()
                    }}
                />
            </Dialog>

            <Dialog
                icon={IconNames.TRASH}
                isOpen={isFolderDeleteModalOpen}
                header={() => <span>Удаление категории</span>}
                onClose={toggleFolderDeleteModal}
            >
                <Query height="150px" promiseFn={() => movaviGateway.getCategory(folderId)}>
                    {response => (
                        <DeleteFolderForm
                            initialModel={FormMappers.categoryToDeleteForm(response)}
                            onClose={toggleFolderDeleteModal}
                            handleSubmit={async () => {
                                const response = await movaviGateway.deleteCategory(folderId)
                                if (!response.safe.ok) return

                                toggleFolderDeleteModal()
                                fileExplorerApi.sync()
                            }}
                        />
                    )}
                </Query>
            </Dialog>
        </Flex>
    )
}

export default HomePage



