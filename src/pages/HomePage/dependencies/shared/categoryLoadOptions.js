import movaviGateway from 'api/movaviGateway'

const loadOptions = async (search, prevOptions, additional) => {
    const response = await movaviGateway.readCategoriesPaged({ page: additional ? additional.prevPage + 1 : 1, perPage: 15, conditions: ['id', 'IS NOT NULL'] })
    if (!response.safe.ok) {
        return {
            options: [],
            hasMore: false
        }
    }

    const result = response.data.result
    const filteredOptions = result.data.map(x => ({ value: x.id, label: x.title }))

    const hasMore = result.page < result.totalPages

    return {
        options: filteredOptions,
        hasMore,
        additional: {
            prevPage: result.page
        }
    };
}

export default loadOptions