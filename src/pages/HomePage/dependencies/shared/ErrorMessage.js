import React from 'react'
import { ErrorMessage as FormikErrorMessage } from 'formik'

const CustomError = props => <FormikErrorMessage {...props} render={msg => <p style={{ color: '#A82A2A' }}>{msg}</p>} />

export default CustomError