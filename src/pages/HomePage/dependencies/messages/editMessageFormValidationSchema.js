import * as Yup from 'yup'

const isHTML = (str) => {
    const fragment = document.createRange().createContextualFragment(str)
    fragment.querySelectorAll('*').forEach(el => el.parentNode.removeChild(el))
    return !(fragment.textContent || '').trim()
}

const editMessageFormValidationSchema = Yup.object().shape({
    category: Yup.object()
        .test({
            message: 'Обязательно',
            test: x => !!x.value
        }),
    title: Yup.string()
        .strict(true)
        .min(2, 'Слишком коротко')
        .max(30, 'Слишком длинно')
        .trim("Пробелы в начале или в конце строки")
        .required('Обязательно'),
    body: Yup.string()
        .strict(true)
        .test({
            message: 'Невалидныая HTML-разметка',
            test: isHTML
        })
        .trim("Пробелы в начале или в конце строки")
        .required('Обязательно')
})

export default editMessageFormValidationSchema