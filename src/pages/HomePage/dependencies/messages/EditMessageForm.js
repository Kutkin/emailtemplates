import React from 'react'
import { Form, withFormik } from 'formik'
import { Button, Intent, InputGroup, TextArea } from "@blueprintjs/core"
import { Flex, Box } from "reflexbox"
import AsyncPaginate from "react-select-async-paginate"

import categoryLoadOptions from '../shared/categoryLoadOptions'
import editMessageFormValidationSchema from './editMessageFormValidationSchema'
import ErrorMessage from '../shared/ErrorMessage'
import { MARGIN } from 'shared/theme'

function EditMessage(props) {
    return (
        <Form>
            <Box mt={MARGIN.NORMAL}>
                <p>Категория</p>
                <AsyncPaginate
                    value={props.values.category}
                    loadOptions={categoryLoadOptions}
                    onChange={category => {
                        props.setValues({ ...props.values, category })
                    }}
                    isSearchable={false}
                />
                <ErrorMessage name="category" />
            </Box>
            <Box mt={MARGIN.NORMAL}>
                <p>Название</p>
                <InputGroup
                    name="title"
                    onChange={props.handleChange}
                    onBlur={props.handleBlur}
                    value={props.values.title}
                />
                <ErrorMessage name="title" />
            </Box>
            <Box mt={MARGIN.NORMAL}>
                <p>Сообщение</p>
                <TextArea
                    name="body"
                    growVertically={true}
                    large={true}
                    fill
                    onChange={props.handleChange}
                    onBlur={props.handleBlur}
                    value={props.values.body}
                    rows={7}
                />
                <ErrorMessage name="body" />
            </Box>

            <Flex
                justifyContent="flex-end"
                mt={MARGIN.NORMAL}
            >
                <Box mr={MARGIN.NORMAL}>
                    <Button
                        text="Отмена"
                        disabled={props.isSubmitting}
                        onClick={props.onClose}
                    />
                </Box>

                <Button
                    type="submit"
                    text="Сохранить"
                    intent={Intent.PRIMARY}
                    disabled={!props.isValid || props.isSubmitting}
                    loading={props.isSubmitting}
                />
            </Flex>
        </Form>
    )
}

const EditMessageForm = withFormik({
    mapPropsToValues: props => props.initialModel,
    validationSchema: editMessageFormValidationSchema,
    handleSubmit: (values, formikBag) => formikBag.props.handleSubmit(values, formikBag)
})(EditMessage)


export default EditMessageForm