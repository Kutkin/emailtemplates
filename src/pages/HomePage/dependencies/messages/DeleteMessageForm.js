import React from 'react'
import { Form, withFormik } from 'formik'
import { Button, Intent } from "@blueprintjs/core"
import { Flex, Box } from "reflexbox"

import { MARGIN } from 'shared/theme'

function DeleteMessage(props) {
    return (
        <Form>
            <p>Вы действительно хотите удалить это сообщение?</p>
            <Flex
                justifyContent="flex-end"
                mt={MARGIN.NORMAL}
            >
                <Box mr={MARGIN.NORMAL}>
                    <Button
                        text="Отмена"
                        disabled={props.isSubmitting}
                        onClick={props.onClose}
                    />
                </Box>

                <Button
                    text="Удалить"
                    intent={Intent.DANGER}
                    disabled={!props.isValid || props.isSubmitting}
                    loading={props.isSubmitting}
                    onClick={() => props.submitForm()}
                />
            </Flex>
        </Form>
    )
}

const DeleteMessageForm = withFormik({
    mapPropsToValues: props => ({}),
    handleSubmit: (values, formikBag) => formikBag.props.handleSubmit()
})(DeleteMessage)


export default DeleteMessageForm