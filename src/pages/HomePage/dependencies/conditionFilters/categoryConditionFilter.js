function isEmptyObject(obj) {
    return Object.keys(obj).length === 0 && obj.constructor === Object
}

function categoryConditionFilter(parentFolderId, createFilter) {
    const predicates = createFilter()

    const parentFolderIdPredicate = parentFolderId ? ["parent", "=", parentFolderId] : ["parent", "IS NULL"]

    if (isEmptyObject(predicates)) {
        return parentFolderIdPredicate
    }

    return {
        operator: 'and',
        ...predicates
    }
}

export default categoryConditionFilter