function isEmptyObject(obj) {
    return Object.keys(obj).length === 0 && obj.constructor === Object
}

function messageConditionFilter(parentFolderId, createFilter) {
    const predicates = createFilter()

    const parentFolderIdPredicate = parentFolderId ? ["category", "=", parentFolderId] : []

    if (isEmptyObject(predicates)) {
        return parentFolderIdPredicate
    }

    return {
        operator: 'and',
        ...predicates
    }
}

export default messageConditionFilter