const FormHelpers = {
    toEditMessageForm([messageResponse, categoryResponse]) {
        const message = messageResponse.data.result.data[0]
        const { id, title } = categoryResponse.data.result.data[0]
        return {
            ...message,
            category: { value: id, label: title }
        }
    },
    toCreateMessageForm(lastRoute) {
        return {
            title: '',
            body: '<html><head></head><body><p>текст</p></body></html>',
            category: lastRoute.id ? { value: lastRoute.id, label: lastRoute.text } : { value: '', label: '' },
        }
    },
    messageToServer({ id, title, body, category: { value } }) {
        return { id, title, body, category: { id: value } }
    },

    categoryToForm([currentCategoryResponse, parentCategoryResponse]) {
        const { id, title } = currentCategoryResponse.data.result.data[0]
        const { id: parentId, title: parentTitle } = parentCategoryResponse ?
            parentCategoryResponse.data.result.data[0]
            : { id: null, title: "" }
        return { id, title, parent: { value: parentId, label: parentTitle } }
    },

    formToCategory({ id, title, parent: { value } }) {
        return { id, title, parent: { id: value } }
    },

    createCategory() {
        return { title: "", parent: { value: null } }
    },

    categoryToDeleteForm(response) {
        const { title, children, messages } = response.data.result.data[0]
        return { title, hasChildren: children.length > 0, hasMessages: messages.length > 0 }
    }
}

export default FormHelpers