import React from 'react'
import { Form, withFormik } from 'formik'
import { Button, Intent, InputGroup } from "@blueprintjs/core"
import { Flex, Box } from "reflexbox"
import AsyncPaginate from "react-select-async-paginate"

import categoryLoadOptions from '../shared/categoryLoadOptions'
import editCategoryFormValidationSchema from './editCategoryFormValidationSchema'
import ErrorMessage from '../shared/ErrorMessage'
import { MARGIN } from 'shared/theme'

const categoryReducer = values => (prev, loaded) => {
    const removeSelf = x => x.value !== values.id
    return [...prev, ...loaded.filter(removeSelf)]
}

function EditMessage(props) {
    return (
        <Form>
            <Box mt={MARGIN.NORMAL}>
                <p>Родительская категория (можно оставить пустой)</p>
                <AsyncPaginate
                    value={props.values.parent}
                    loadOptions={categoryLoadOptions}
                    reduceOptions={categoryReducer(props.values)}
                    onChange={category => {
                        props.setValues({ ...props.values, parent: category })
                    }}
                    isSearchable={false}
                />
                <ErrorMessage name="parent" />
            </Box>
            <Box mt={MARGIN.NORMAL}>
                <p>Название</p>
                <InputGroup
                    name="title"
                    onChange={props.handleChange}
                    onBlur={props.handleBlur}
                    value={props.values.title}
                />
                <ErrorMessage name="title" />
            </Box>

            <Flex
                justifyContent="flex-end"
                mt={MARGIN.NORMAL}
            >
                <Box mr={MARGIN.NORMAL}>
                    <Button
                        text="Отмена"
                        disabled={props.isSubmitting}
                        onClick={props.onClose}
                    />
                </Box>

                <Button
                    type="submit"
                    text="Сохранить"
                    intent={Intent.PRIMARY}
                    disabled={!props.isValid || props.isSubmitting}
                    loading={props.isSubmitting}
                />
            </Flex>
        </Form>
    )
}

const EditMessageForm = withFormik({
    mapPropsToValues: props => {
        return props.initialModel
    },
    validationSchema: editCategoryFormValidationSchema,
    handleSubmit: (values, formikBag) => formikBag.props.handleSubmit(values, formikBag),
})(EditMessage)


export default EditMessageForm