import * as Yup from 'yup'

const editCategoryFormValidationSchema = Yup.object().shape({
    title: Yup.string()
        .strict(true)
        .min(2, 'Слишком коротко')
        .max(20, 'Слишком длинно')
        .trim("Пробелы в начале или в конце строки")
        .required('Обязательно'),
})

export default editCategoryFormValidationSchema