import React from 'react'
import { Form, withFormik } from 'formik'
import { Button, Intent, Alert, Icon, Spinner } from "@blueprintjs/core"
import { IconNames } from "@blueprintjs/icons"
import { Flex, Box } from "reflexbox"

import { MARGIN } from 'shared/theme'

function DeleteCategory(props) {
    const [isAlertOpen, setAlert] = React.useState(false)
    return (
        <Form>
            <p>Вы хотите удалить категорию <strong>{props.values.title}</strong>?</p>
            <Flex
                justifyContent="flex-end"
                mt={MARGIN.NORMAL}
            >
                <Box mr={MARGIN.NORMAL}>
                    <Button
                        text="Отмена"
                        disabled={props.isSubmitting}
                        onClick={props.onClose}
                    />
                </Box>

                <Button
                    text="Удалить"
                    intent={Intent.DANGER}
                    disabled={!props.isValid || props.isSubmitting}
                    loading={props.isSubmitting}
                    onClick={() => {
                        (props.values.hasChildren || props.values.hasMessages) ?
                            setAlert(true) :
                            props.submitForm()
                    }}
                />
            </Flex>
            <Alert
                cancelButtonText="Отмена"
                confirmButtonText="Продолжить"
                icon={IconNames.TRASH}
                intent={Intent.DANGER}
                isOpen={isAlertOpen}
                onCancel={() => setAlert(false)}
                onConfirm={() => {
                    setAlert(false)
                    props.submitForm()
                }}
            >
                <p>Вы действительно хотите удалить <strong>{props.values.title}</strong>?</p>
                {props.values.hasChildren && <p>Она содержит <strong>дочерние папки</strong>.</p>}
                {props.values.hasMessages && <p>Она содержит <strong>сообщения</strong>.</p>}
            </Alert>
        </Form>
    )
}

const DeleteCategoryForm = withFormik({
    mapPropsToValues: props => {
        return props.initialModel
    },
    handleSubmit: (values, formikBag) => formikBag.props.handleSubmit(),
})(DeleteCategory)


export default DeleteCategoryForm