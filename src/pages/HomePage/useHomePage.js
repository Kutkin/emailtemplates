import React from 'react'

import movaviGateway from 'api/movaviGateway'
import useFileExplorer from 'components/FileExplorer/useFileExplorer'
import useFiltersManager from 'components/FiltersManager/useFiltersManager'
import useUpdateEffect from 'shared/useUpdateEffect'
import Filter from 'components/FiltersManager/models/Filter'
import categoryConditionFilter from 'pages/HomePage/dependencies/conditionFilters/categoryConditionFilter'
import messageConditionFilter from 'pages/HomePage/dependencies/conditionFilters/messageConditionFilter'

const initialState = {
    isFileEditModalOpen: false,
    fileEditId: null,

    isFileCreateModalOpen: false,

    isFileDeleteModalOpen: false,

    isFolderEditModalOpen: false,
    folderId: null,

    isFolderAddModalOpen: false,

    isFolderDeleteModalOpen: false,
}

const TOGGLE_FILE_EDIT_MODAL = 'TOGGLE_FILE_EDIT_MODAL'
const SET_FILE_EDIT_ID = 'SET_FILE_EDIT_ID'
const TOGGLE_FILE_ADD_MODAL = 'TOGGLE_FILE_ADD_MODAL'
const TOGGLE_FILE_DELETE_MODAL = 'TOGGLE_FILE_DELETE_MODAL'
const TOGGLE_FOLDER_EDIT_MODAL = 'TOGGLE_FOLDER_EDIT_MODAL'
const SET_FOLDER_ID = 'SET_FOLDER_ID'
const TOGGLE_FOLDER_ADD_MODAL = 'TOGGLE_FOLDER_ADD_MODAL'
const TOGGLE_FOLDER_DELETE_MODAL = 'TOGGLE_FOLDER_DELETE_MODAL'

function reducer(state, action) {
    switch (action.type) {

        case TOGGLE_FILE_EDIT_MODAL:
            return { ...state, isFileEditModalOpen: !state.isFileEditModalOpen }

        case SET_FILE_EDIT_ID:
            return { ...state, fileEditId: action.payload }

        case TOGGLE_FILE_ADD_MODAL:
            return { ...state, isFileAddModalOpen: !state.isFileAddModalOpen }

        case TOGGLE_FILE_DELETE_MODAL:
            return { ...state, isFileDeleteModalOpen: !state.isFileDeleteModalOpen }

        case TOGGLE_FOLDER_EDIT_MODAL:
            return { ...state, isFolderEditModalOpen: !state.isFolderEditModalOpen }

        case SET_FOLDER_ID:
            return { ...state, folderId: action.payload }

        case TOGGLE_FOLDER_ADD_MODAL:
            return { ...state, isFolderAddModalOpen: !state.isFolderAddModalOpen }

        case TOGGLE_FOLDER_DELETE_MODAL:
            return { ...state, isFolderDeleteModalOpen: !state.isFolderDeleteModalOpen }

        default: throw new Error("Неверный action")
    }
}

function useHomePage() {
    const [state, dispatch] = React.useReducer(reducer, initialState)

    const categoryConditionsApi = useFiltersManager([
        new Filter({ name: 'findCategoryByName' }),
        new Filter({ name: 'findCategoryMessagesCount' }),
    ])

    const messageConditionsApi = useFiltersManager([
        new Filter({ name: 'findMessageByName' })
    ])

    const fileExplorerApi = useFileExplorer({
        perPage : 10,
        folderPromiseFn: ({ parentFolderId, ...rest }) => {
            return movaviGateway.readCategoriesPaged({
                conditions: categoryConditionFilter(parentFolderId, categoryConditionsApi.createFilter),
                ...rest
            })
        },
        folderReduceItems: items => items.map(x => ({ ...x, messagesCount: x.messages.length })),
        filePromiseFn: ({ parentFolderId, ...rest }) => {
            return movaviGateway.readMessagesPaged({
                conditions: messageConditionFilter(parentFolderId, messageConditionsApi.createFilter),
                ...rest
            })
        }
    })

    React.useEffect(() => {
        fileExplorerApi.folderPagination.setPaging()
        fileExplorerApi.filePagination.setPaging()
    }, [])

    useUpdateEffect(() => {
        fileExplorerApi.reset()
    }, [categoryConditionsApi.updateHash, messageConditionsApi.updateHash])

    return {
        ...state,

        toggleFileEditInputModal() {
            dispatch({ type: TOGGLE_FILE_EDIT_MODAL })
        },

        openFileEditModal(id) {
            dispatch({ type: SET_FILE_EDIT_ID, payload: id })
            dispatch({ type: TOGGLE_FILE_EDIT_MODAL })
        },

        toggleFileAddInputModal() {
            dispatch({ type: TOGGLE_FILE_ADD_MODAL })
        },

        toggleFileDeleteModal() {
            dispatch({ type: TOGGLE_FILE_DELETE_MODAL })
        },

        openFileDeleteModal(id) {
            dispatch({ type: SET_FILE_EDIT_ID, payload: id })
            dispatch({ type: TOGGLE_FILE_DELETE_MODAL })
        },

        openFolderEditModal(id) {
            dispatch({ type: SET_FOLDER_ID, payload: id })
            dispatch({ type: TOGGLE_FOLDER_EDIT_MODAL })
        },

        toggleFolderEditModal() {
            dispatch({ type: TOGGLE_FOLDER_EDIT_MODAL })
        },

        toggleFolderAddModal() {
            dispatch({ type: TOGGLE_FOLDER_ADD_MODAL })
        },

        toggleFolderDeleteModal() {
            dispatch({ type: TOGGLE_FOLDER_DELETE_MODAL })
        },

        openFolderDeleteModal(id) {
            dispatch({ type: SET_FOLDER_ID, payload: id })
            dispatch({ type: TOGGLE_FOLDER_DELETE_MODAL })
        },

        openFolder(id, title) {
            categoryConditionsApi.resetFilters()
            fileExplorerApi.hierarchy.refreshRoutes(id, title)
        },

        categoryConditionsApi,
        messageConditionsApi,

        fileExplorerApi,
    }
}

export default useHomePage
