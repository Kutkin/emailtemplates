import React from 'react'

const useUpdateEffect = (effect, deps) => {
    const isInitialMount = React.useRef(true)

    React.useEffect(() => {
        if (isInitialMount.current) {
            isInitialMount.current = false
        } else {
            return effect()
        }
    }, deps)
}

export default useUpdateEffect