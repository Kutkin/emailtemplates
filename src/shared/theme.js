const PALETTE = {
    PRIMARY: '#EBF1F5',
    SECONDARY: '#F5F8FA'
}

const MARGIN = {
    SMALL: '0.5rem',
    NORMAL: '1rem',
    LARGE: '2rem',
}

export {
    PALETTE,
    MARGIN
}